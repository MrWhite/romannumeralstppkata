import org.junit.jupiter.api.Test

class romanNumeralsTest {

    @Test
    fun returnsIforOne(){
        val romanNum = romanNumerals()
        println("Index : 1 and expected Answer: I")
        assert(romanNum.convert(1).equals("I"))

    }

}